# Terminando Game Activity

Ahora vamos a terminar el trabajo que comenzamos poniéndolo todo junto en _GameActivity_. Lo primero en nuestra agenda es agregar la vista tetris recién creada al diseño de la actividad del juego. Podemos agregar fácilmente TetrisView como elemento secundario en cualquier lugar dentro de un archivo de diseño utilizando la etiqueta <com.mydomain.tetris.views.TetrisView>:

``` xml
            <LinearLayout
				android:layout_width="0dp"
				android:layout_height="match_parent"
				android:layout_weight="9">
				<!--	Adding	TetrisView	-->
				<com.mydomain.tetris.views.TetrisView
					android:id="@+id/view_tetris"
					android:layout_width="match_parent"
					android:layout_height="match_parent"	/>
```

Una vez que haya agregado la vista tetris a *activity_game.xml*, abra la clase _GameActivity_ y emplea los cambios a la clase que se muestra en la siguiente bloque de código:

``` kotlin
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.miguelgonzalez.tetris.models.AppModel
import com.example.miguelgonzalez.tetris.storage.AppPreferences
import com.example.miguelgonzalez.tetris.view.TetrisView

class GameActivity : AppCompatActivity() {

    var	tvHighScore: TextView? = null
    var	tvCurrentScore: TextView? =	null
    private	lateinit var tetrisView: TetrisView
    var	appPreferences: AppPreferences? = null
    private val appModel: AppModel = AppModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        appPreferences = AppPreferences(this)
        appModel.setPreferences(appPreferences)

        val btnRestart = findViewById<Button>(R.id.btn_restart)
        tvHighScore	= findViewById<TextView>(R.id.tv_high_score)
        tvCurrentScore = findViewById<TextView>(R.id.tv_current_score)
        tetrisView = findViewById<TetrisView>(R.id.view_tetris)
        tetrisView.setActivity(this)
        tetrisView.setModel(appModel)

        tetrisView.setOnTouchListener(this::onTetrisViewTouch)
        btnRestart.setOnClickListener(this::btnRestartClick)

        updateHighScore()
        updateCurrentScore()
    }

    private fun btnRestartClick(view: View) {
        appModel.restartGame()
    }

    private fun onTetrisViewTouch(view: View, event: MotionEvent):
    Boolean {
        if (appModel.isGameOver() || appModel.isGameAwaitingStart()) {
            appModel.startGame()
            tetrisView.setGameCommandWithDelay(AppModel.Motions.DOWN)
        } else if(appModel.isGameActive()) {
            when (resolveTouchDirection(view, event)) {
                0 -> moveTetromino(AppModel.Motions.LEFT)
                1 -> moveTetromino(AppModel.Motions.ROTATE)
                2 -> moveTetromino(AppModel.Motions.DOWN)
                3 -> moveTetromino(AppModel.Motions.RIGHT)
            }
        }
        return	true
    }

    private fun resolveTouchDirection(view: View, event: MotionEvent):
    Int	{
        val x = event.x / view.width
        val	y = event.y	/ view.height
        val	direction: Int
        direction =	if (y > x) {
            if (x > 1 - y) 2 else 0
        }
        else {
            if (x >	1 -	y) 3 else 1
        }
        return	direction
    }

    private fun moveTetromino(motion: AppModel.Motions) {
        if (appModel.isGameActive()) {
            tetrisView.setGameCommand(motion)
        }
    }

    private fun	updateHighScore() {
        tvHighScore?.text = "${appPreferences?.getHighScore()}"
    }

    private	fun	updateCurrentScore() {
        tvCurrentScore?.text = "0"

    }
}

```

Una vez terminado siéntete libre de jugar con el juego que creaste. ¡Te lo mereces!

![Terminado](../resources/terminado.png)
