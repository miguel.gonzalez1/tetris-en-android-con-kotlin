# Creando el modelo de la aplicación

Similar a los modelos que hemos creado hasta ahora, necesitamos un archivo de clase separado para el modelo de aplicación. Continúa y crea un nuevo archivo Kotlin llamado _AppModel.kt_ y agregue una clase llamada _AppModel_ al archivo con importaciones para _Point_, _FieldConstants_, _array2dOfByte_ y _AppPreferences_:

``` kotlin
import android.graphics.Point
import com.mydomain.tetris.constants.FieldConstants
import com.mydomain.tetris.helpers.array2dOfByte
import com.mydomain.tetris.storage.AppPreferences

class AppModel{}
```
son para realizar un seguimiento de la puntuación actual, el estado del campo de juego tetris, el bloque actual, el estado actual del juego y los movimientos experimentados por el bloque actual. AppModel también debe tener acceso directo a los valores almacenados en el archivo SharedPreferences de la aplicación a través de la clase AppPreferences que creamos.

Lo primero que debemos hacer es agregar las constantes necesarias que serán utilizadas por AppModel. Necesitaremos crear constantes para los posibles estados de juego y los posibles movimientos que pueden ocurrir durante el juego. Estas constantes se crean con facilidad con el uso de clases enum:

``` kotlin
class AppModel {
	enum class Statuses {
		AWAITING_START, ACTIVE, INACTIVE, OVER
	}
	
	enum class Motions {
		LEFT, RIGHT, DOWN, ROTATE
	}
}
```

Habiendo agregado las constantes requeridas, podemos proceder agregando las propiedades de clase necesarias de AppModel, que son las siguientes:

``` kotlin
    var score: Int = 0
    private var preferences: AppPreferences? = null
	var currentBlock: Block? = null
	var currentState: String = Statuses.AWAITING_START.name
	
	private var field: Array<ByteArray> = array2dOfByte(
		FieldConstants.ROW_COUNT.value,
		FieldConstants.COLUMN_COUNT.value
	)
```

A continuación debemos agregar algunas funciones de setter y getter. Estas funciones son setPreferences (), setCellStatus () y getCellStatus (). Agrega las siguientes funciones a AppModel:

``` kotlin
    fun setPreferences(preferences: AppPreferences?) {
        this.preferences = preferences
    }

    fun	getCellStatus(row: Int, column: Int): Byte? {
        return field[row][column]
    }

    private fun setCellStatus(row: Int, column: Int, status: Byte?) {
        if (status != null) {
            field[row][column] = status
        }
    }
```

Las funciones para verificar el estado también son necesarias en el modelo. Estos servirán como medio para afirmar el estado actual del juego. Como tenemos tres estados de juego posibles correspondientes a tres estados de juego posibles, se requieren tres funciones para cada estado de juego individual. Estos métodos son isGameAwaitingStart (), isGameActive () y isGameOver ():

``` kotlin
    fun	isGameOver(): Boolean {
        return currentState == Statuses.OVER.name
    }

    fun	isGameActive(): Boolean {
        return currentState == Statuses.ACTIVE.name
    }

    fun	isGameAwaitingStart(): Boolean {
        return currentState == Statuses.AWAITING_START.name
    }
```

Agreguemos una función que se pueda usar para aumentar el valor de puntaje que tiene la puntuación. Nombraremos la función boostScore().

``` kotlin
    private fun boostScore() {
        score += 10
        if (score > preferences?.getHighScore() as Int)
            preferences?.saveHighScore(score)
    }
```
Una vez que los campos y las funciones básicas están en funcionamiento, podemos avanzar para crear funciones un poco más complicadas. La primera de estas funciones es generateNextBlock ():

``` kotlin
    private	fun	generateNextBlock() {
        currentBlock = Block.createBlock()
    }
```

Antes de continuar con las definiciones de métodos, vamos a crear una clase de enumeración más para mantener valores de celda constantes. Crea un archivo CellConstants.kt en el paquete _constants_ y agregue el siguiente código fuente:

``` kotlin
enum class CellConstants(val value: Byte) {
	EMPTY(0), EPHEMERAL(1)
}
```
Importe la clase enum recién creada en AppModel. La utilizaremos en la próxima función:

``` kotlin
    private fun validTranslation(position: Point, shape: Array<ByteArray>): Boolean {
        return if (position.y < 0 || position.x < 0) {
            false
        } else if (position.y + shape.size > FieldConstants.ROW_COUNT.value) {
            false
        } else if (position.x + shape[0].size > FieldConstants.COLUMN_COUNT.value) {
            false
        } else {
            for (i in 0 until shape.size) {
                for	(j in 0 until shape[i].size) {
                    val	y = position.y + i
                    val	x =	position.x + j
                    if (CellConstants.EMPTY.value != shape[i][j] && CellConstants.EMPTY.value != field[y][x]) {
                        return	false
                    }
                }
            }
            true
        }
    }
```

Necesitamos una función de llamada para _validTranslation()_. Declararemos _moveValid()_ para cumplir con este propósito. Agregue la siguiente función a AppModel:

``` kotlin
    private	fun	moveValid(position: Point, frameNumber: Int?): Boolean {
        val shape: Array<ByteArray>? = currentBlock?.getShape(frameNumber as Int)
        return validTranslation(position, shape as Array<ByteArray>)
    }
```
_moveValid()_ utiliza _validTranslation()_ para verificar si un movimiento realizado por el jugador está permitido. Si el movimiento está permitido, devuelve verdadero, de lo contrario se devuelve falso. Necesitamos crear algunos otros métodos importantes. Estos son _generaField()_, _resetField()_, _persistCellData()_, _evalField()_, _translateBlock()_, _blockAdditionPossible()_, _shiftRows()_, _startGame()_, _restartGame()_, _endGame()_ y _resetModel()_.

``` kotlin
    fun generateField(action: String) {
        if	(isGameActive()) {
            resetField()
            var frameNumber: Int? =	currentBlock?.frameNumber
            val coordinate: Point? = Point()
            coordinate?.x =	currentBlock?.position?.x
            coordinate?.y =	currentBlock?.position?.y
            when (action) {
                Motions.LEFT.name -> {
                    coordinate?.x = currentBlock?.position?.x?.minus(1)
                }
                Motions.RIGHT.name -> {
                    coordinate?.x = currentBlock?.position?.x?.plus(1)
                }
                Motions.DOWN.name -> {
                    coordinate?.y = currentBlock?.position?.y?.plus(1)
                }
                Motions.ROTATE.name -> {
                    frameNumber = frameNumber?.plus(1)
                    if (frameNumber != null) {
                        if (frameNumber >= currentBlock?.frameCount as Int) {
                            frameNumber = 0
                        }
                    }
                }
            }
            if (!moveValid(coordinate as Point, frameNumber)) {
                translateBlock(currentBlock?.position as Point, currentBlock?.frameNumber as Int)
                if (Motions.DOWN.name == action) {
                    boostScore()
                    persistCellData()
                    assessField()
                    generateNextBlock()
                    if (!blockAdditionPossible()) {
                        currentState = Statuses.OVER.name;
                        currentBlock = null
                        resetField(false)
                    }
                }
            } else {
                if (frameNumber != null) {
                    translateBlock(coordinate, frameNumber)
                    currentBlock?.setState(frameNumber,	coordinate)
                }
            }
        }
    }

    private fun resetField(ephemeralCellsOnly: Boolean = true) {
        for	(i in 0 until FieldConstants.ROW_COUNT.value) {
            (0 until FieldConstants.COLUMN_COUNT.value).filter { !ephemeralCellsOnly || field[i][it] == CellConstants.EPHEMERAL.value }.forEach	{ field[i][it] = CellConstants.EMPTY.value }
        }
    }

    private	fun	persistCellData() {
        for	(i in 0 until field.size)	{
            for (j in 0 until field[i].size) {
                var	status = getCellStatus(i, j)
                if (status == CellConstants.EPHEMERAL.value) {
                    status = currentBlock?.staticValue
                    setCellStatus(i, j, status)
                }
            }
        }
    }

    private fun assessField() {
        for	(i in 0 until field.size) {
            var	emptyCells = 0;
            for	(j in 0 until field[i].size) {
                val	status = getCellStatus(i,	j)
                val	isEmpty	= CellConstants.EMPTY.value == status
                if	(isEmpty)
                    emptyCells++
            }
            if (emptyCells == 0)
                shiftRows(i)
        }
    }

    private fun translateBlock(position: Point, frameNumber: Int) {
        synchronized(field)	{
            val	shape: Array<ByteArray>? = currentBlock?.getShape(frameNumber)
            if (shape != null)	{
                for	(i in shape.indices) {
                    for	(j in 0 until shape[i].size) {
                        val	y = position.y + i
                        val	x =	position.x + j
                        if (CellConstants.EMPTY.value != shape[i][j]) {
                            field[y][x]	= shape[i][j]
                        }
                    }
                }
            }
        }
    }

    private	fun	blockAdditionPossible(): Boolean {
        if (!moveValid(currentBlock?.position as Point, currentBlock?.frameNumber))	{
            return	false
        }
        return	true
    }

    private fun shiftRows(nToRow: Int) {
        if (nToRow > 0) {
            for	(j in nToRow - 1 downTo	0) {
                for	(m in 0	until field[j].size) {
                    setCellStatus(j + 1, m, getCellStatus(j, m))
                }
            }
        }
        for (j in 0 until field[0].size) {
            setCellStatus(0, j, CellConstants.EMPTY.value)
        }
    }

    fun startGame() {
        if (!isGameActive()) {
            currentState = Statuses.ACTIVE.name
            generateNextBlock()
        }
    }

    fun restartGame() {
        resetModel()
        startGame()
    }

    fun endGame() {
        score = 0
        currentState = AppModel.Statuses.OVER.name
    }

    private fun resetModel() {
        resetField(false)
        currentState = Statuses.AWAITING_START.name
        score = 0
    }
```
