# Creando la UI

Antes de enbarcanos en el proceso de codificar una interfaz de usuario, prodría ser de ayuda el hacer una representación grafica de la UI (User Interface) a implementar. Esto puede llevarse a cabo con diferentes herramientas, tales como Photoshop, pero en este caso un simple sketch es suficiente:

![Sketch](../resources/sketch.png)

Podemos observar que necesitaremos dos pantallas, la pantalla principal y la pantalla donde se dearrolla el juego. Estas pantallas requerirán dos actividades separadas. Llamaremos a estas actividades _MainActivity_ y _GameActivity_.

## Implementando los layouts

Ahora que tenemos una idea de las actividades que son necesarias podemos iniciar su implementación. En Android Studio crea un nuevo proyecto con el nombre de _Tetris_ elige una actividad vacía y deja por defecto las demás opciones. Te debio crear un archivo *activity_main.xml* que debe verse muy similar a esto 

```xml
<?xml	version="1.0"	encoding="utf-8"?><android.support.constraint.ConstraintLayout	xmlns:android="h
	xmlns:app="http://schemas.android.com/apk/res-auto"
	xmlns:tools="http://schemas.android.com/tools"
	android:layout_width="match_parent"
	android:layout_height="match_parent"
	tools:context="com.mydomain.tetris.MainActivity">
</android.support.constraint.ConstraintLayout>
```
Donde el _ConstraintLayout_ es un tipo vista de agrupación que permite un posicionamiento y redimensionamiento flexible de los widgets de una aplicación.

De nuestro sketch podemos ver que todos los elementos estan colocados de forma vertical. Podemos hacer eso con el uso de un _LinearLayout_:

```xml
<?xml	version="1.0"	encoding="utf-8"?><android.support.constraint.ConstraintLayout	xmlns:android="h
	xmlns:app="http://schemas.android.com/apk/res-auto"
	xmlns:tools="http://schemas.android.com/tools"
	android:layout_width="match_parent"
	android:layout_height="match_parent"
	tools:context="com.mydomain.tetris.MainActivity">
	    <LinearLayout
			android:layout_width="match_parent"
			android:layout_height="match_parent"
			app:layout_constraintBottom_toBottomOf="parent"
			app:layout_constraintLeft_toLeftOf="parent"
			app:layout_constraintRight_toRightOf="parent"
			app:layout_constraintTop_toTopOf="parent"
			android:layout_marginVertical="16dp"
			android:orientation="vertical">
		</LinearLayout>
</android.support.constraint.ConstraintLayout>
```
## Definiendo los recursos de dimensiones

Tipicamente en un archivo layout, podemos tener numerosos elementos que especifican los mismo valores de restricciones para atributos. Esos valores deben ser agregados a un archivo de recursos de dimensiones. 

En tu proyecto navega hasta la carpeta res | values y crea un nuevo archivo de recursos y nombralo como _dimens_.

Abre el archivo y agrega algunas dimensiones:

``` xml
<?xml	version="1.0"	encoding="utf-8"?>
<resources>
	<dimen	name="layout_margin_top">16dp</dimen>
	<dimen	name="layout_margin_bottom">16dp</dimen>
	<dimen	name="layout_margin_start">16dp</dimen>
	<dimen	name="layout_margin_end">16dp</dimen>
	<dimen	name="layout_margin_vertical">16dp</dimen>
</resources>
```

Ahora que hemos añadido algunas dimensiones, podemos utilizarlas en nuestro linear layout:

``` xml
<android.support.constraint.ConstraintLayout	xmlns:android="http://schemas.android.com/apk/res/andr
	xmlns:app="http://schemas.android.com/apk/res-auto"
	xmlns:tools="http://schemas.android.com/tools"
	android:layout_width="match_parent"
	android:layout_height="match_parent"
	tools:context="com.mydomain.tetris.MainActivity">
	<LinearLayout
		android:layout_width="match_parent"
		android:layout_height="match_parent"
		app:layout_constraintBottom_toBottomOf="parent"
		app:layout_constraintLeft_toLeftOf="parent"
		app:layout_constraintRight_toRightOf="parent"
		app:layout_constraintTop_toTopOf="parent"
		android:layout_marginTop="@dimen/layout_margin_top"	
<!—	layout_margin_top	dimension	reference	—>
		android:layout_marginBottom="@dimen/layout_margin_bottom"	
<!—	layout_margin_top	dimension	reference	—>
		android:orientation="vertical"
		android:gravity="center_horizontal">
	</LinearLayout>
</android.support.constraint.ConstraintLayout>
```
Ahora que hemos colocado nuestro agrupador de vistas LinearLayout necesitamos agregar los View necesarios, pero antes entendamos algunos conceptos.

### View

Un View es un elemento de un layout que ocupa un área de la pantalla y es responsable de dibujarse y el manejo de eventos. Los View son la base para una UI. Los View pueden ser creados dentro de un archivo XML y programados dentro de un componente dentro de una clase.

Ahora agregamos lo siguiente:

``` xml
<?xml	version="1.0"	encoding="utf-8"?>
<android.support.constraint.ConstraintLayout	xmlns:android="http://schemas.android.com/apk/res/andr
	xmlns:app="http://schemas.android.com/apk/res-auto"
	xmlns:tools="http://schemas.android.com/tools"
	android:layout_width="match_parent"
	android:layout_height="match_parent"
	tools:context="com.mydomain.tetris.MainActivity">
	<LinearLayout
		android:layout_width="match_parent"
		android:layout_height="match_parent"
		app:layout_constraintBottom_toBottomOf="parent"
		app:layout_constraintLeft_toLeftOf="parent"
		app:layout_constraintRight_toRightOf="parent"
		app:layout_constraintTop_toTopOf="parent"
		android:layout_marginTop="@dimen/layout_margin_top"
		android:layout_marginBottom="@dimen/layout_margin_bottom"
		android:orientation="vertical">
		<TextView
			android:layout_width="wrap_content"
			android:layout_height="wrap_content"
			android:text="TETRIS"
			android:textSize="80sp"/>
		<TextView
			android:id="@+id/tv_high_score"
			android:layout_width="wrap_content"
			android:layout_height="wrap_content"
			android:text="High	score:	0"
			android:textSize="20sp"
			android:layout_marginTop="@dimen/layout_margin_top"/>
		<LinearLayout
			android:layout_width="match_parent"
			android:layout_height="0dp"
			android:layout_weight="1"
			android:orientation="vertical">
			<Button
				android:id="@+id/btn_new_game"
				android:layout_width="wrap_content"
				android:layout_height="wrap_content"
				android:text="New	game"/>
			<Button
				android:id="@+id/btn_reset_score"
				android:layout_width="wrap_content"
				android:layout_height="wrap_content"
				android:text="Reset	score"/>
			<Button											
			    android:id="@+id/btn_exit"
				android:layout_width="wrap_content"
				android:layout_height="wrap_content"
				android:text="exit"/>
		</LinearLayout>
	</LinearLayout>
</android.support.constraint.ConstraintLayout>
```

![screens](../resources/screens.png)

Podemos ver que algo no se ve como en el Sketch, nuestros objetos no estan centrados. Podemos resolver esto utilizando el atributo _android:gravity_.

``` xml
<LinearLayout
	android:layout_width="match_parent"
	android:layout_height="match_parent"
	app:layout_constraintBottom_toBottomOf="parent"
	app:layout_constraintLeft_toLeftOf="parent"
	app:layout_constraintRight_toRightOf="parent"
	app:layout_constraintTop_toTopOf="parent"
	android:layout_marginTop="@dimen/layout_margin_top"
	android:layout_marginBottom="@dimen/layout_margin_bottom"
	android:orientation="vertical"
	android:gravity="center">	
	<!--	Aligns	child	elements	to	the	centre	of	view	group -->
	
	
	<LinearLayout
		android:layout_width="match_parent"
		android:layout_height="0dp"
	    android:layout_weight="1"
		android:orientation="vertical"
		android:gravity="center">	
    <!--	Aligns	child	elements	to	the	centre	of	view	group		-->
```

Con esto los elementos deben estar centrados:

![centered](../resources/centered.png)

### Definiendo los recursos string
 
Hasta ahora, hemos pasado cadenas codificadas como valores a atributos de elementos que requieren que se establezca el texto. Por defecto el archivo para strings se encuentra en res | values | strings.xml

Lo abrimos y agregamos algunas strings:

``` xml
<resources>
	<string	name="app_name">Tetris</string>
	<string	name="high_score_default">High	score:	0</string>
	<string	name="new_game">New	game</string>
	<string	name="reset_score">Reset	score</string>		
	<string	name="exit">exit</string>
</resources>
```

Y modificamos nuestro archivo *activity_main.xml*

``` xml
<TextView
    android:layout_width="wrap_content"
	android:layout_height="wrap_content"
	android:text="@string/app_name"
	android:textAllCaps="true"
	android:textSize="80sp"/>
<TextView
	android:id="@+id/tv_high_score"
	android:layout_width="wrap_content"
	android:layout_height="wrap_content"
	android:text="@string/high_score_default"
	android:textSize="20sp"
	android:layout_marginTop="@dimen/layout_margin_top"/>
	
	
	<Button
		android:id="@+id/btn_new_game"
		android:layout_width="wrap_content"
		android:layout_height="wrap_content"
		android:text="@string/new_game"/>
	<Button
		android:id="@+id/btn_reset_score"
    	android:layout_width="wrap_content"
		android:layout_height="wrap_content"
		android:text="@string/reset_score"/>
	<Button
		android:id="@+id/btn_exit"
		android:layout_width="wrap_content"
		android:layout_height="wrap_content"
		android:text="@string/exit"/>
	
```

### Manejando los eventos de entrada

En el ciclo de interacción de un usuario con una aplicación, un medio por el cual el usuario puede proporcionar algún tipo de información para la ejecución de un proceso es interactuar con un widget. Estas entradas pueden ser capturadas con eventos. En las aplicaciones de Android, los eventos se capturan por el objeto View especifico con el que él usuario interactúa. La clase de View proporciona las estructuras y procedimientos necesarios para el manejo de eventos de entrada.

Un ejemplo sería:

``` kotlin
class	MainActivity	:	AppCompatActivity()	{
	override	fun	onCreate(savedInstanceState:	Bundle?)	{
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		val	btnExit:	Button	=	findViewById<Button>(R.id.btn_exit)
		btnExit.setOnClickListener(this::handleExitEvent)
	}
	
	fun	handleExitEvent(view:	View)	{
		finish()
	}
}
```

Ahora podemos implementar las siguientes funciones para los clicks de los botones:

``` kotlin
import	android.content.Intent
import	android.support.v7.app.AppCompatActivity
import	android.os.Bundle
import	android.view.View
import	android.widget.Button
import	android.widget.TextView
class	MainActivity	:	AppCompatActivity()	{
	var	tvHighScore:	TextView?	=	null
	override	fun	onCreate(savedInstanceState:	Bundle?)	{
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		supportActionBar?.hide()
		
		val	btnNewGame	=	findViewById<Button>(R.id.btn_new_game)
		val	btnResetScore	=	findViewById<Button>(R.id.btn_reset_score)
		val	btnExit	=	findViewById<Button>(R.id.btn_exit)
		tvHighScore	=	findViewById<TextView>(R.id.tv_high_score)
		btnNewGame.setOnClickListener(this::onBtnNewGameClick)
		btnResetScore.setOnClickListener(this::onBtnResetScoreClick)
		btnExit.setOnClickListener(this::onBtnExitClick)
	}
	
	private	fun	onBtnNewGameClick(view:	View)	{
		val	intent	=	Intent(this,	GameActivity::class.java)
		startActivity(intent)
	}
	
	private	fun	onBtnResetScoreClick(view:	View)	{}
	
	private	fun	onBtnExitClick(view:	View)	{
		System.exit(0)
	}
```

No se te olvide crear una nueva actividad vacía con el nombre de _GameActivity_ ya que al dar click al boton de New Game se lanzará la otra actividad