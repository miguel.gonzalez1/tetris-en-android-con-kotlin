# Creando Tetris View

Hemos implementado con éxito clases para modelar bloques, marcos y formas de diferentes tetrominos que se utilizarán dentro de la aplicación, así como implementado una clase de AppModel para coordinar todas las interacciones entre las vistas y estos componentes programáticos creados. Sin este View existente, no hay ningún medio por el cual un usuario pueda interactuar con AppModel. Si un usuario no puede interactuar con el juego, es posible que el juego no exista. En esta sección, implementaremos TetrisView, la interfaz de usuario mediante la cual un usuario jugará Tetris.

Crea un paquete llamado view en su paquete fuente y agregue un archivo TetrisView.kt en él. Como queremos que TestrisView sea un View, debemos declararlo para extender la clase de View. Agregue el siguiente código a TetrisView.kt.

``` kotlin
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.Handler
import android.os.Message
import android.util.AttributeSet
import android.view.View
import android.widget.Toast
import com.example.miguelgonzalez.tetris.constants.CellConstants
import com.example.miguelgonzalez.tetris.GameActivity
import com.example.miguelgonzalez.tetris.constants.FieldConstants
import com.example.miguelgonzalez.tetris.models.AppModel
import com.example.miguelgonzalez.tetris.models.Block

class TetrisView : View	{

    private	val	paint =	Paint()
    private	var	lastMove: Long = 0
    private	var	model: AppModel? = null
    private	var	activity: GameActivity? = null
    private	val	viewHandler = ViewHandler(this)
    private	var	cellSize: Dimension	= Dimension(0, 0)
    private	var	frameOffset: Dimension = Dimension(0, 0)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)
    companion object {
        private val DELAY = 500
        private val BLOCK_OFFSET = 2
        private val FRAME_OFFSET_BASE = 10
    }
```

La clase TetrisView se declara para extender de View. La View es una clase que todos los elementos de View de la aplicación deben extender. Como el tipo de View tiene un constructor que debe inicializarse, estamos declarando dos constructores secundarios para TetrisView que inicializan dos constructores distintos de la clase de View, dependiendo de qué constructor secundario se llame.

## Implementando ViewHandler

Como los bloques se moverán a lo largo de los campos en intervalos con un retraso constante, necesitamos un medio para poner el hilo que controla el movimiento de los bloques para dormir y despertar el hilo para hacer un movimiento de bloque después de un período de tiempo. Una buena manera de cumplir con este requisito es usar un controlador para procesar las solicitudes de demora de mensajes y continuar con el manejo de los mensajes una vez que se haya completado la demora. Al poner esto en términos más directos, según la documentación de Android, el controlador le permite enviar y procesar los objetos de mensaje asociados con el MessageQueue de un hilo. Cada instancia de controlador está asociada con un subproceso y la cola de mensajes del subproceso.

_ViewHandler_ es un controlador personalizado que implementaremos para TetrisView que satisface las necesidades de procesamiento y envío de mensajes de la vista. Como ViewHandler es una subclase de Handler, debemos extender Handler y agregar nuestro comportamiento necesario a la clase ViewHandler. Agregue la siguiente clase de VieHandler como clase privada dentro de TetrisView:

``` kotlin
    private class ViewHandler(private val owner: TetrisView) : Handler() {
        override fun handleMessage(message: Message) {
            if (message.what == 0) {
                if (owner.model != null) {
                    if (owner.model!!.isGameOver()) {
                        owner.model?.endGame()
                        Toast.makeText(owner.activity, "Game over", Toast.LENGTH_LONG).show()
                    }
                    if (owner.model!!.isGameActive()) {
                        owner.setGameCommandWithDelay(AppModel.Motions.DOWN)
                    }
                }
            }
        }
        fun	sleep(delay: Long) {
            this.removeMessages(0)
            sendMessageDelayed(obtainMessage(0), delay)
        }
    }
```

## Implementando Dimensions

_Dimensions_ solo necesita ser capaz de mantener dos propiedades: ancho y alto. Como tal, es un candidato perfecto para la utilización de una clase de datos. Agregue la siguiente clase privada a la clase TetrisView:

``` kotlin
private data class Dimension(val width: Int, val height: Int)
```

## Implementando Tetris View

Como habrás adivinado, en este punto TetrisView está lejos de completarse. En primer lugar, debemos implementar algunos métodos de establecimiento para el modelo y las propiedades de actividad de la vista. Estos métodos se muestran a continuación. Asegúrate de agregarlos a tu clase TetrisView.

``` kotlin
    fun setModel(model: AppModel) {
        this.model = model
    }

    fun	setActivity(gameActivity: GameActivity)	{
        this.activity = gameActivity
    }
```

Ahora, agreguemos tres métodos adicionales _setGameCommand()_, _setGameCommandWithDelay()_ y _updateScore()_.

``` kotlin
    fun setGameCommand(move: AppModel.Motions) {
        if (null !=	model && (model?.currentState == AppModel.Statuses.ACTIVE.name)) {
            if (AppModel.Motions.DOWN == move) {
                model?.generateField(move.name)
                invalidate()
                return
            }
            setGameCommandWithDelay(move)
        }
    }

    fun setGameCommandWithDelay(move: AppModel.Motions) {
        val	now	= System.currentTimeMillis()
        if	(now - lastMove	> DELAY) { model?.generateField(move.name)
            invalidate()
            lastMove = now
        }
        updateScores()
        viewHandler.sleep(DELAY.toLong())
    }

    private fun updateScores() {
        activity?.tvCurrentScore?.text = "${model?.score}"
        activity?.tvHighScore?.text	= "${activity?.appPreferences?.getHighScore()}"
    }
```

El método _invalidate()_ al que se llama dentro de _setGameCommand()_ se puede tomar como una solicitud para dibujar un cambio en la pantalla. _invalidate()_ finalmente resulta en una llamada a _onDraw()_.

_onDraw()_ es un método que se hereda de la clase View. Se llama cuando una vista debe representar su contenido. Necesitamos proporcionar una implementación personalizada de esto para nuestra vista. Agregue el siguiente código a su clase TetrisView.

``` kotlin
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawFrame(canvas)
        if	(model != null) {
            for	(i in 0 until FieldConstants.ROW_COUNT.value) {
                for	(j in 0 until FieldConstants.COLUMN_COUNT.value) {
                    drawCell(canvas, i, j)
                }
            }
        }
    }

    private fun drawFrame(canvas: Canvas) {
        paint.color = Color.LTGRAY
        canvas.drawRect(frameOffset.width.toFloat(), frameOffset.height.toFloat(), width -	frameOffset.width.toFloat(), height - frameOffset.height.toFloat(),	paint)
    }

    private fun drawCell(canvas: Canvas, row: Int, col: Int) {
        val	cellStatus = model?.getCellStatus(row, col)
        if (CellConstants.EMPTY.value != cellStatus) {
            val	color = if (CellConstants.EPHEMERAL.value == cellStatus) {
                model?.currentBlock?.color
            } else {
                Block.getColor(cellStatus as Byte)
            }
            drawCell(canvas, col, row, color as Int)
        }
    }

    private fun drawCell(canvas: Canvas, x: Int, y: Int, rgbColor: Int) {
        paint.color = rgbColor
        val	top: Float = (frameOffset.height + y * cellSize.height + BLOCK_OFFSET).toFloat()
        val	left: Float = (frameOffset.width + x * cellSize.width + BLOCK_OFFSET).toFloat()
        val	bottom: Float = (frameOffset.height + (y + 1) * cellSize.height - BLOCK_OFFSET).toFloat()
        val	right: Float = (frameOffset.width + (x + 1) * cellSize.width - BLOCK_OFFSET).toFloat()
        val	rectangle =	RectF(left,	top, right,	bottom)
        canvas.drawRoundRect(rectangle, 4F, 4F, paint)
    }

    override fun onSizeChanged(width: Int, height: Int, previousWidth: Int, previousHeight: Int) {
        super.onSizeChanged(width, height, previousWidth, previousHeight)
        val cellWidth = (width - 2 * FRAME_OFFSET_BASE) / FieldConstants.COLUMN_COUNT.value
        val cellHeight = (height - 2 * FRAME_OFFSET_BASE) / FieldConstants.ROW_COUNT.value
        val n = Math.min(cellWidth,	cellHeight)
        this.cellSize = Dimension(n, n)
        val offsetX	= (width - FieldConstants.COLUMN_COUNT.value * n) / 2
        val offsetY	= (height - FieldConstants.ROW_COUNT.value * n) / 2
        this.frameOffset = Dimension(offsetX, offsetY)
    }
```

